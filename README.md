# Intro till Sencha Ext JS

Dokumentation för Ext JS 4.2: [docs.sencha.com/extjs/4.2.1](http://docs.sencha.com/extjs/4.2.1/)

Utbildningsledarens API för dummy-data:
```
#!bash
# Exempel: Lista fängelser
http://correctional-care.appspot.com/user/api/list/prisons

# Exempel: Listning av "klienter" på häkten/fängelser
http://correctional-care.appspot.com/user/api/list/clients

# Exempel: Lista häkten
http://correctional-care.appspot.com/user/api/list/custodies

# Utdrag av tillgängliga query-parametrar: 
# * page
# * page_size
# * ancestor
# * offset
# * filter_by
# * order_by

# Exempel: Lista enbart anhållna på Kriminalvårdens Bomhus-häkte och max 10 st.
http://correctional-care.appspot.com/user/api/list/clients?page_size=10&ancestor=ahNzfmNvcnJlY3Rpb25hbC1jYXJlchMLEgdDdXN0b2R5IgZCb21odXMM
```

För att komma runt webbläsarens CORS-restriktioner i anslutning till utvecklingsarbete, öppna Chrome med *--disable-web-security*-flaggan. E.g.
```
#!bash
$ open /Applications/Google\ Chrome.app/ --args --disable-web-security
```

## 1. Ladda ner Sencha Cmd och Ext JS
Ladda ner [Sencha Cmd](http://www.sencha.com/products/sencha-cmd/download) för ditt OS och installera programvaran.

Ladda ner [Ext JS v. 4.2.1 (GPL)](http://www.sencha.com/products/extjs/download/ext-js-4.2.1/2281) och extrahera biblioteket till mappen *sencha-ext-js* i din utvecklings-root.

## 2. Skapa en ´´workspace´´
```
#!bash
$ sencha --sdk-path ../sencha-ext-js/ generate workspace workspace
```

## 3. Generera en boilerplate-app i din ´´workspace´´
```
#!bash
$ cd workspace/
$ sencha --sdk-path ext generate app ThemeDemoApp theme-demo-app
```

## 4. Starta Sencha Cmd:s webbserver och besök din app
```
#!bash
$ sencha fs web [-port 8000] start -map theme-demo-app/
```

## 5. Generera ett eget *theme*
```
#!bash
$ sencha generate theme my-custom-theme
```

## 6. Kompilera din Sencha-app
```
#!bash
$ sencha app build
```

## 7. Ladda ner Sencha Architect och skapa en app i Ext JS

Trial-programvara: [Sencha Architect](http://www.sencha.com/products/architect/download/)

För att skapa ett Sencha ID: [Sencha Forum](http://www.sencha.com/forum/) 
